#!/bin/bash
# Resize a picture to a smaller dimension for uploading to flickr
# Use geeqie's keyboard shortcuts to map this.
dim=$( identify $1 | awk '{ split($3, dim, "x"); x=dim[1]; y=dim[2]; if (x > y) print 1920; else print 1280; }' )

# ln -s $1 best/
convert $1 -quality 85 -resize $dim -depth 8 flickr/$1
